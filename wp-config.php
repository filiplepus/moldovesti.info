<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'moldoves_wp');

if($_SERVER['HTTP_HOST']=='moldovesti.io' || $_SERVER['HTTP_HOST']=='www.moldovesti.io')
{
    define('DB_USER', 'root');

    define('DB_PASSWORD', '');

    define('WP_HOME','http://moldovesti.io');
    define('WP_SITEURL','http://moldovesti.io');
}else{

/** MySQL database username */
define('DB_USER', 'moldoves_wp');

/** MySQL database password */
define('DB_PASSWORD', 'C2<J+>0');
}
/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'cMJuOMKg53yFkgNwljI4pAPbXKyBLBsnLjKi_WV92fZ7GC0Gc0NREKaa7TEwRTVW');
define('SECURE_AUTH_KEY',  'l5nhdNwXUaK4Pt8DvcqfZy8l82XqD9uxVq0Mn7FOjbqy59cIHtKhltJDhLMr1Bbx');
define('LOGGED_IN_KEY',    '2xk0K5DaDfN5cIXiqhcd5Q9IlrDQsRvDWUXFFm9cleFL34KMO8Q_hEvT8U9KQkST');
define('NONCE_KEY',        '9cxcxT9YZmN2Bncybuvo6P8c63LEQ8a3o1dBgLJtBv5qlSSghg2BUQCldRTnWf4T');
define('AUTH_SALT',        '_dJhDYU4_wYTXGkGzFDCAZCT9z75hdhgBhPIJ2eRZ41rEneFE8uSQCDU2xGxm7Vw');
define('SECURE_AUTH_SALT', 'sU8LaNQnBwW7Vb3l3YclmLYop8FB9zPRsNEk1Cq7GEkgDyVYbqkdcqdQjzWvWQj0');
define('LOGGED_IN_SALT',   'H9oA3rR_QeJcZPGxRABoFGh2uV9P8UEzYbzuPyMspQ4Uk4o26toeKgibD6jOLqj4');
define('NONCE_SALT',       '8RyXv3HzrUKodmd9DhzO3PI5fumwb1JN7pa85KhGSx4V5WraAHsq2iBAQtNdo2Lf');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
