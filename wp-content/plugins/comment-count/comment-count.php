<?php
/*
Plugin Name: Comment Count
Plugin URI: http://wordpress.org/extend/plugins/comment-count/
Description: Counts the total number of comments.
Version: 1.1
Author: Nick Momrik
Author URI: http://nickmomrik.com/
*/

function mdv_comment_count() {
    global $wpdb;
	$request = "SELECT COUNT(*) FROM $wpdb->comments WHERE comment_approved = '1'";

    echo $wpdb->get_var($request);
}
?>
